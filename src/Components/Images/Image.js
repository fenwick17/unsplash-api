import React from 'react'; 

const Image = ({ image, altInfo, link, photographer, photographerName, profileImage }) => (
  <div>
    <img src={image} alt={altInfo}/>
    <p>Photo by <a href={'https://unsplash.com/@' + photographerName}>{photographer}</a></p>
    <img src={profileImage} alt="Profile logo" />
  </div>
)

export default Image;
