import React, { Component } from 'react';
import Unsplash, { toJson } from 'unsplash-js';
import styled from 'styled-components';

import Image from './Image';

const unsplash = new Unsplash({
  applicationId: process.env.REACT_APP_API_KEY,
  secret: process.env.REACT_APP_API_SECRET
});

const ImageLayout = styled.div`
`

class ImageContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      photos: [],
      pages: 1,
      imagesNeeded: 25,
    }

    this.getImages = this.getImages.bind(this);
    this.getMoreImages = this.getMoreImages.bind(this);
    this.loadMore = this.loadMore.bind(this);
  }

  componentDidMount() {
    this.getImages();
    window.addEventListener('scroll', this.checkScroll, false);
  }

  getImages() {
    unsplash.photos.listPhotos(this.state.page, this.state.imagesNeeded, "latest")
      .then(toJson)
      .then(json => {
        this.setState({ photos: json })
      });
  }

  getMoreImages() {
    unsplash.photos.listPhotos(this.state.pages, this.state.imagesNeeded, "latest")
      .then(toJson)
      .then(json => {
        this.setState((state) => {
          return { photos: state.photos.concat(json) };
        });
      })
  }

  loadMore() {
    this.setState(state => ({ 
      pages: state.pages + 1,
      imagesNeeded: state.imagesNeeded + 25,
    }), () => {
      this.getMoreImages();
    })
  }

  render() {
    return (
      <div>
        <ImageLayout>
          { this.state.photos.map(item =>
          <Image 
            image={item.urls.small}
            link={item.links.html}
            altInfo={item.alt_description}
            photographer={item.user.name}
            photographerName={item.user.username}
            profileImage={item.user.profile_image.small}
          />
          )}
        </ImageLayout>
        <button onClick={this.loadMore}>Load More Images</button>
      </div>
    )
  }
}

export default ImageContainer;
