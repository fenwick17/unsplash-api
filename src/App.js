import React, { Component } from 'react';
import styled from 'styled-components';
import ImageContainer from './Components/Images/ImageContainer';
import './App.css';

const Container = styled.div`
  margin: 0 auto;
  max-width: 1240px;
`

class App extends Component {
  render() {
    return (
      <Container>
        <h1>Welcome to Unsplash</h1>
        <ImageContainer />
      </Container>
    );
  }
}

export default App;
